# 基于Gradio YOLOv5 Det v0.4 HuggingFace Demo的自定义接口脚本
# 创建人：曾逸夫
# 创建时间：2022-05-31

import os

os.system("pip install gradio==3.0.9")

import gradio as gr

demo = gr.Interface.load(name="spaces/Zengyf-CVer/Gradio_YOLOv5_Det_v4")

demo.launch()
